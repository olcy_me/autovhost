#!/bin/bash
echo -e "\e[1;31m This is red text \e[0m"
# Check if user is sudo
if [ "$EUID" -ne 0 ]; then
  echo "Please run as root"
  exit
fi

############
## COLORS
############
RED=$'\e[1;31m'
NC=$'\033[0m'

# shellcheck disable=SC2162
read -p "Non de domaine :" domaine

# Check if folder already exist
DIR="/var/www/${domaine}"
if [ -d "$DIR" ]; then
  echo "${RED}Dossier déjà éxistant :${NC} ${DIR}..."
  exit
else
  #Création du dossier du projet
  mkdir /var/www/"${domaine}"
  sudo chmod g+w /var/www/${domaine}
fi

echo "${domaine}.local"

# Selection du template de vhost
echo "###############"
echo "Type de projet :"
echo "1 - Prestashop"
echo "2 - Wordpress"
echo "3 - Joomla"
echo "4 - Laravel"
echo "###############"

# shellcheck disable=SC2162
read -p "Choix vhost : " typeVhost

echo "Copie du vhost"

if [[ ${typeVhost} -eq 1 ]]
then
 sudo cp -v -i /home/olcy/Documents/autovhost/prestashop /etc/nginx/sites-enabled/"${domaine}"
elif [[ ${typeVhost} -eq 2 ]]
then
  sudo cp -v -i /home/olcy/Documents/autovhost/wordpress /etc/nginx/sites-enabled/"${domaine}"
elif [[ ${typeVhost} -eq 3 ]]
then
  sudo cp -v -i /home/olcy/Documents/autovhost/joomla.txt /etc/nginx/sites-enabled/"${domaine}"
elif [[ ${typeVhost} -eq 4 ]]
then
  sudo cp -v -i /home/olcy/Documents/autovhost/laravel /etc/nginx/sites-enabled/"${domaine}"
fi

# Remplissage du vhost
sed -i 's/domainReplace/'${domaine}'/g' /etc/nginx/sites-enabled/${domaine}

# Sélection de la version de php
echo "Sélection de la version de PHP :"
sudo ls /etc/php/

read -p "Version PHP : " phpVersion
sed -i 's/phpReplace/'${phpVersion}'/g' /etc/nginx/sites-enabled/${domaine}

echo "Redémarrage de nginx"
sudo service nginx reload

echo "Création de la base de donnée..."

sudo mysql -uroot -e "CREATE USER ${domaine}@'%' IDENTIFIED BY 'T3st@T3st';"
sudo mysql -uroot -e "CREATE DATABASE ${domaine} /*\!40100 DEFAULT CHARACTER SET utf8 *;"
sudo mysql -uroot -e "FLUSH PRIVILEGES;"

echo "Base de donnée créé"

# Inscrition dans le fichier hosts
echo "Inscription du domaine dans le fichier hosts"
echo "\nLine2" >> /etc/hosts
